# Py3ContextManager

<b>A class to make Python debugging easier, with concise exception notifications printed to screen or log.</b>

----

This is a debugger/context manager for Python3.<br>
Development = obvious exceptions, and I hate how messy the code looks with try/except clauses everywhere. With this class you can simply add "with ease: " before any line you suspect to misbehave.<br>
You can choose to print all exceptions to the terminal, or to append them to a log file.<br>
An instance of this class can (optionally) be called with "dangerLevel" (1: Critical, 2: Trivial) as well as a "contextComment".<br>
The "self.debugLevel" setting takes the following values: 0: Debug off, 1: Critical exceptions, 2: All exceptions


<b>Examples:</b><br>

Code:

```
with ease: x = 1 / 0
if ease.ex:
    print("Gasp! How could this possibly go wrong!?")
```
Output:
```
  ________________________________
_/ Context Manager - Exception #1 \_____________________________________________

Potentially Troublesome Exception

Type: "ZeroDivisionError"
Value: "division by zero"
Doc string: "Second argument to a division or modulo operation was zero."
Context Comment: N/A
File Name: ContextManager.py
Line Number 52:

with ease: x = 1 / 0
________________________________________________________________________________

Gasp! How could this possibly go wrong!?

```
<br><br>Code:
```
with ease(1,"If this function ever breaks, call the president asap!"):
    NuclearTimeMachineCodeGenerator = 423542062354 ** "A 1989 Ford Fiesta"
```
Output:
```
  ________________________________
_/ Context Manager - Exception #2 \_____________________________________________

Potentially Troublesome Exception

Type: "TypeError"
Value: "unsupported operand type(s) for ** or pow(): 'int' and 'str'"
Doc string: "Inappropriate argument type."
Context Comment: If this function ever breaks, call the president asap!
File Name: ContextManager.py
Line Number 57:

NuclearTimeMachineCodeGenerator = 423542062354 ** "A 1989 Ford Fiesta"
________________________________________________________________________________
```
<br><br>Code:
```
with ease(2,"I know about the f*****g exception, capeesh? /Tony"):
    something = fuggedaboutit
```
Output:
```
  ________________________________
_/ Context Manager - Exception #3 \_____________________________________________

Mostly Harmless Exception

Type: "NameError"
Value: "name 'fuggedaboutit' is not defined"
Doc string: "Name not found globally."
Context Comment: I know about the f*****g exception, capeesh? /Tony
File Name: ContextManager.py
Line Number 60:

something = fuggedaboutit
________________________________________________________________________________
```
