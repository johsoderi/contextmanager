#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sys import stdout
import os.path
from textwrap import dedent
class ContextManager:
    ''' This is a debugger/context manager. Development = obvious exceptions, and
    I hate how messy the code looks with try/except clauses everywhere. With this
    class you can simply add "with ease:" before any line you suspect to misbehave.
    You can choose to print all exceptions to the terminal, or to append them to a
    log file. An instance of this class can be called with "dangerLevel"
    (1: Critical, 2: Trivial) and a "contextComment". See examples below.
    The "self.debugLevel" setting takes a value between 0-2:
    0: Debug off, 1: Critical exceptions, 2: All exceptions '''
    def __init__(self, dLev=1, comm="N/A"):
        self.debugLevel = 2
        #self.log = "/path/to/log"
        self.log = None
        self.contextComment,self.dangerLevel,self.ct, self.ex = comm, dLev, 0, False
        self.levels = [f"\nSchrödingers Exception             \n",
                       f"\nPotentially Troublesome Exception  \n",
                       f"\nMostly Harmless Exception          \n"]
    def __call__(self,dangerLevel,contextComment):
        self.dangerLevel,self.contextComment = dangerLevel,contextComment
        return self
    def __enter__(self): self.ct += 1
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ex = True if (exc_type != None) else False
        if (exc_type == None): self.ct -= 1
        if (exc_type != None) and (self.debugLevel >= self.dangerLevel):
            if self.log: l = open(self.log, "a+")
            fileName = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(f"\n  {'_'*31}{'_'*(len(str(self.ct)))}", file=l if self.log else stdout)
            print(f"_/ Context Manager - Exception #{self.ct} ",end='', file=l if self.log else stdout)
            print(f"\\{'_'*(46-(len(str(self.ct))))}",end='', file=l if self.log else stdout)
            print(f"\n{self.levels[self.dangerLevel]}", file=l if self.log else stdout)
            print(f"Type: \"{exc_type.__name__}\"\nValue: \"{exc_val}\"", file=l if self.log else stdout)
            print(f"Doc string: \"{exc_val.__doc__}\"", file=l if self.log else stdout)
            print(f"Context Comment: {self.contextComment}", file=l if self.log else stdout)
            print(f"File Name: {fileName}", file=l if self.log else stdout)
            print(f"Line Number {exc_tb.tb_lineno}:\n", file=l if self.log else stdout)
            with open(__file__,"r") as s:
                print(dedent(s.readlines()[exc_tb.tb_lineno-1].rstrip()), file=l if self.log else stdout)
            print(f"{'_'*80}\n", file=l if self.log else stdout)
            if self.log: l.close()
        return True
ease = ContextManager()

# Examples:
with ease: x = 1 / 0
if ease.ex:
    print("Gasp! How could this possibly go wrong!?")

with ease(1,"If this function ever breaks, call the president asap!"):
    NuclearTimeMachineCodeGenerator = 423542062354 ** "A 1989 Ford Fiesta"

with ease(2,"I know about the f*****g exception, capeesh? /Tony"):
    something = fuggedaboutit
